import './index.scss';

const Footer = () => `
    <footer>
        <div class="container">
            <p>FE Challenge by Rita Leitão</p>
        </div>
    </footer>
`;

export default Footer;